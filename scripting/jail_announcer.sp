
#include <csgo_colors>
#include <devzones>
#define VERSION "1.0.49"
public Plugin myinfo =
{
	name		= "Announcer",
	author		= "ShaRen",
	description	= "Show Rules",
	version		= VERSION,
	url			= "Servers-Info.Ru"
}

//bool g_bAllInJails;								// все ли Т в джайлах?
int g_iSeconds;
Handle hTimerCheckJRepeat	= null;				// Проверить все ли Т ещё в джайлах
Handle hTimerLeave			= null;				// Предупреждение о том что надо вывести Т
Handle hTimerOpen			= null;				// Предупреждение о том что надо открыть джайлы

public void OnPluginStart() {
	HookEvent("round_start", eV_round_start, EventHookMode_PostNoCopy);
}

public void eV_round_start(Event event, const char[] name, bool DB)
{
	//g_bAllInJails = false;
	g_iSeconds = 0;
	if(hTimerCheckJRepeat != null)		delete hTimerCheckJRepeat;
	if(hTimerOpen		  != null)		delete hTimerOpen;
	if(hTimerLeave		  != null)		delete hTimerLeave;
	hTimerCheckJRepeat	= CreateTimer( 3.0, LeaveCheckJRepeat, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	hTimerOpen			= CreateTimer(55.0, LeaveOpenStart, _, TIMER_FLAG_NO_MAPCHANGE);
	hTimerLeave			= CreateTimer(80.0, LeaveTimeStart, _, TIMER_FLAG_NO_MAPCHANGE);
}

public Action LeaveCheckJRepeat(Handle timer)
{
	//g_bAllInJails = false;
	if(IsEmptyJails() || !Zone_CheckIfZoneExists("jail", false)) {
		return Plugin_Stop;
	}
	for (int i=1; i<=MaxClients; i++)
		if (IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 )
			if (!Zone_IsClientInZone(i, "jail", false)) {
				hTimerCheckJRepeat = null;
				return Plugin_Stop;
			}
	//g_bAllInJails = true;
	return Plugin_Continue;
}

public Action LeaveOpenStart(Handle timer)
{
	if (!IsEmptyJails()) {
		CreateTimer(1.0, OpenTimeRepeat, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		g_iSeconds = 5;
	}
	hTimerOpen = null;
	return Plugin_Stop;
}

public Action LeaveTimeStart(Handle timer)
{
	if (!IsEmptyJails()) {
		CreateTimer(1.0, LeaveTimeRepeat, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
		g_iSeconds = 10;
	}
	hTimerLeave = null;
	return Plugin_Stop;
}

public Action OpenTimeRepeat(Handle timer)
{
	if(IsEmptyJails())
		return Plugin_Stop;
	if (g_iSeconds > 0) {
		g_iSeconds--;
		for(int i=1; i<=MaxClients; i++)
			if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 3)
				PrintHintText(i, "Не забудьте открыть камеры (%i сек)", g_iSeconds);
	}
	if(!g_iSeconds) return Plugin_Stop;
	return Plugin_Continue;
}

public Action LeaveTimeRepeat(Handle timer)
{
	if(IsEmptyJails())
		return Plugin_Stop;
	if (g_iSeconds > 0) {
		g_iSeconds--;
		for(int i=1; i<=MaxClients; i++)
			if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 3)
				PrintHintText(i, "Не забудьте вывести заключенных (%i сек)", g_iSeconds);
	}
	if(!g_iSeconds) return Plugin_Stop;
	return Plugin_Continue;
}

bool IsEmptyJails()
{
	if (!Zone_CheckIfZoneExists("jail", false))
		return false;
	for (int i=1; i<MaxClients; i++)
		if (IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == 2 )
			if (Zone_IsClientInZone(i, "jail", false))
				return false;
	return true;
}

